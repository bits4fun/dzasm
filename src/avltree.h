
/* -------------------------------------------------------------------------------------------------

    DDDDDDDDDDDDD            ZZZZZZZZZZZZZZZZ
    DDDDDDDDDDDDDDD        ZZZZZZZZZZZZZZZZ
    DDDD         DDDD               ZZZZZ
    DDDD         DDDD             ZZZZZ
    DDDD         DDDD           ZZZZZ             AAAAAA         SSSSSSSSSSS   MMMM       MMMM
    DDDD         DDDD         ZZZZZ              AAAAAAAA      SSSS            MMMMMM   MMMMMM
    DDDD         DDDD       ZZZZZ               AAAA  AAAA     SSSSSSSSSSS     MMMMMMMMMMMMMMM
    DDDD         DDDD     ZZZZZ                AAAAAAAAAAAA      SSSSSSSSSSS   MMMM MMMMM MMMM
    DDDDDDDDDDDDDDD     ZZZZZZZZZZZZZZZZZ     AAAA      AAAA           SSSSS   MMMM       MMMM
    DDDDDDDDDDDDD     ZZZZZZZZZZZZZZZZZ      AAAA        AAAA  SSSSSSSSSSS     MMMM       MMMM

    (C) Copyright Gunther Strube (gstrube@gmail.com), 1996-2016

    This file is part of DZasm.

    DZasm is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation;
    either version 2, or (at your option) any later version.
    DZasm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with DZasm;
    see the file COPYING. If not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


typedef struct avlnode {
                        short           height;         /* height of avltree (max search levels from node) */
                        void            *data;          /* pointer to data of node */
                        struct avlnode  *left, *right;  /* pointers to left and right avl subtrees */
                       } avltree;

void    move(avltree **p, avltree **newroot, int  (*symcmp)(void *,void *));
void    copy(avltree *p, avltree **newroot, int  (*symcmp)(void *,void *), void  *(*create)(void *));
void    deletenode(avltree **root, void *key, int  (*comp)(void *,void *), void (*delkey)(void *));
void    deleteall(avltree **p, void (*deldata)(void *));
void    insert(avltree **root, void *key, int  (*comp)(void *,void *));
void    *find(avltree *p, void *key, int  (*comp)(void *,void *));
void    *reorder(avltree *p, int  (*symcmp)(void *,void *));
void    inorder(avltree *p, void  (*action)(void *));
void    preorder(avltree *p, void  (*action)(void *));
